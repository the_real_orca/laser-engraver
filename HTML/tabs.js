function init_tabs() {
	// add event listener for content titles
	$(".tabs > .title").on("click", function(event){
		var title = $(event.target);
		var id = title.attr("for");
		title.siblings().removeClass("active");
		// activate titles
		title.addClass("active");
		// activate container
		$("#"+id).addClass("active");
	});

	// next tab buttons
	$(".tabs .next").on("click", function(event){
		var activeTab = $(event.target).closest(".content");
		var next = activeTab.next(".title");
		next.trigger("click");
	});
	
	// init active tabs
	$(".tabs > .title.active").trigger("click");
}