![Laser Engraver Logo][logo]
[logo]: https://bytebucket.org/the_real_orca/laser-engraver/raw/50035ec870dc6e217a6fd31e027ba903e14849a9/HTML/images/LaserEncraver.png

Enhancements for NEJE Laser Engraver
=====================================

Features
--------

### Rasperry Pi Web Frontend for NEJE Laser Engraver
* upload image to laser engraver
* adjust engraving area 
* live video from USB WebCam
* progress and estimated time of engraving
* adjust focus (HW update required)
 

### HW enhancements
* housing for laser
* WebCam mounting
* focus adjustment via SW


![Rendered Image](https://bytebucket.org/the_real_orca/laser-engraver/raw/427c1633d18c6b2704cfa66512e6012f4c7b8f2f/HTML/images/CADImage.png)

The Laser Engraver is able to engrave images with 500x500 pixels. With a resolution of **13.4 px/mm** or 340 dpi.
Actually a 512x512 image is sent to the laser, but the top 12 lines and right 12 columns are not printable.


Install
-------

### Install Raspbian to SD card
Download and install **Raspbian Jessie LITE** to SD card.
(https://www.raspberrypi.org/downloads/raspbian/)
 
### Connect to Raspberry Pi
Use a ssh terminal to connect to your Raspberry Pi. (default user: pi / password: raspberry)

Change password:

	passwd
 
### Update System
Update the system and reboot your Raspberry Pi. This may take a while.

	sudo apt-get update
	sudo apt-get -y upgrade
	sudo reboot

### Download and Install Laser Engraver

Download the laser engraver source code and start **setup.sh**.

This will install all dependencies (Python, OpenCV, ...) and install the laser engraver deamon to be automatically started.

	wget https://bitbucket.org/the_real_orca/laser-engraver/get/master.gz
	tar -xzf master.gz
	cd the_real_orca-laser-engraver*
	./setup.sh 

	
### Test Web Server
Try to connect to your Raspberry Pi with a web browser.

### Optimize for headless server (optional steps)

#### Add Ramdisk and Network-Drive

make /temp a ramdrive

	sudo su -c 'echo "tmpts /tmp tmpfs defaults,noatime,nosuid,mode=0777,size=100m 0 0" >> /etc/fstab'

add a network drive

	sudo su -c 'echo "//<server>/<drive>  /mnt/<...>/  cifs  defaults,credentials=/root/.smbcredentials,uid=1000,gid=100,dir_mode=0775,file_mode=0775  0 0" >> /etc/fstab'

Don't forget to create **/root/.smbcredentials**

	username=<user>
	password=<pwd>

and set the correct permissions

	sudo chmod 600 /root/.smbcredentials
	sudo chown root:root /root/.smbcredentials

	
#### disable swap drive

	sudo service dphys-swapfile status
	sudo swapoff -a
	free
	sudo service dphys-swapfile stop
	sudo apt-get purge dphys-swapfile

	
#### rsyslog remove xconsole

open **/etc/rsyslog.conf** and mark the whole xconsole block as comment

	# The named pipe /dev/xconsole ...
	# ...
	#daemon.*;...
	#...
	#...|/dev/xconsole

** enable automatic updates **

	sudo apt-get install unattended-upgrades
	sudo dpkg-reconfigure -plow unattended-upgrades
	cat /etc/apt/apt.conf.d/20auto-upgrades
	sudo unattended-upgrades --dry-run
	tail /var/log/unattended-upgrades/unattended-upgrades.log

#### raspiconfig

	sudo raspi-config 
	
** set graphics memory to none **

	-->  Advanced Options --> Memory Split --> 16MB

	
** Expand SD card **

	--> Expand Filesystem

	

References
----------
http://axengineering.wordpress.com/2016/06/14/neje-laser-engraver-on-linux/
https://github.com/lurch/rpi-serial-console
