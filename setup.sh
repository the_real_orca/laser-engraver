#!/bin/bash
COLOR_BLUE='\e[0;34m'
COLOR_CYAN='\e[0;36m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
TEXT_BOLD='\e[1m'
TEXT_NORMAL='\e[0m'
TEXT_LINK='\033[4;36m'

# install dependencies
echo 
echo -e ${TEXT_BOLD}"install dependencies ..."${TEXT_NORMAL}
sudo apt-get install -y python2.7-dev python-setuptools python-pip  python-opencv python-numpy libopencv-dev python-imaging python-rpi.gpio iptables-persistent

sudo pip install --upgrade pip
sudo pip install pyserial bottle


# copy resources
echo -e ${TEXT_BOLD}"copy files ..."${TEXT_NORMAL}
sudo mkdir -p /usr/share/laser/HTML
sudo cp -rf RaspberryPi/laser.py /usr/share/laser/laser.py
sudo cp -rf HTML/ /usr/share/laser
sudo chmod 0755 /usr/share/laser/laser.py
sudo mkdir -p /usr/share/laser/HTML/uploads
sudo chmod 0777 /usr/share/laser/HTML/uploads

# enable build-in UART
echo -e ${TEXT_BOLD}"enable build-in UART ..."${TEXT_NORMAL}
# Raspberry Pi
sudo systemctl stop serial-getty@ttyAMA0.service
sudo systemctl disable serial-getty@ttyAMA0.service
# Raspberry Pi 3
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
sudo gpasswd --add pi dialout
sudo gpasswd --add pi tty
sudo rpi-serial-console enable
# make backup but do not overwrite
sudo cp -n /boot/cmdline.txt /boot/cmdline.bak
sudo sh -c 'echo "dwc_otg.lpm_enable=0 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait" > /boot/cmdline.txt'
sudo cp -n /boot/config.txt /boot/config.bak
sudo sh -c 'echo "enable_uart=1" >> /boot/config.txt'


# install daemon
echo -e ${TEXT_BOLD}"install daemon..."${TEXT_NORMAL}
sudo service laser stop  2> /dev/null
sudo cp -rf RaspberryPi/laser.sh /etc/init.d/laser.sh
sudo chmod 0755 /etc/init.d/laser.sh
sudo rm -f /var/run/laser.pid 2> /dev/null
sudo update-rc.d laser.sh defaults 2> /dev/null
sudo systemctl daemon-reload
sudo service laser start
#sudo service laser status

# redirect port 80 to 8080
sudo iptables -t nat -D PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080 2> /dev/null
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo netfilter-persistent save 2> /dev/null

echo 
echo -e ${TEXT_BOLD}"Web Frontend for NEJE Laser Engraver is installed"${TEXT_NORMAL}
echo -e "go to ${TEXT_LINK}http://"`hostname -I`"${TEXT_NORMAL}  or  ${TEXT_LINK}http://"`hostname -A`${TEXT_NORMAL}
echo 
